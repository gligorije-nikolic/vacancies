package tfzr.vacancies.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tfzr.vacancies.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	Page<User> findAll(Pageable pageable);
	List<User> findByEmailLike(String email);
}
