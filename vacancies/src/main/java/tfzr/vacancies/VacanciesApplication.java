package tfzr.vacancies;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

@SpringBootApplication
public class VacanciesApplication extends SpringBootServletInitializer {
	public static void main(String[] args) throws Exception {
	    SpringApplication.run(VacanciesApplication.class, args);
	}
	
	@Override
	protected SpringApplicationBuilder configure(
			SpringApplicationBuilder builder) {

		return builder.sources(VacanciesApplication.class);
	}
}
