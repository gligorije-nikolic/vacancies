package tfzr.vacancies.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tfzr.vacancies.model.Vacancy;
import tfzr.vacancies.service.VacancyService;
import tfzr.vacancies.web.dto.VacancyDTO;

@RestController
@RequestMapping("api/vacancies")
public class ApiVacancyController {

	@Autowired
	private VacancyService vacancyService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<VacancyDTO>> getVacancies(
			@RequestParam(value = "jobTitle", required = false) String jobTitle,
			@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "pageSize", required = true) int pageSize) {
		List<VacancyDTO> vacancies = new ArrayList<>();
		int totalPages = 0;
		long totalElements = 0;
		
		if(jobTitle == null){
			Page<Vacancy> vacanciesPage = vacancyService.findAll(page, pageSize);
		for (Vacancy v : vacanciesPage) {
			vacancies.add(new VacancyDTO(v));
		}
		totalPages = vacanciesPage.getTotalPages();
		totalElements = vacanciesPage.getTotalElements();
		}
		
		else{
		for (Vacancy v : vacancyService.findByJobTitle(jobTitle)){
			vacancies.add(new VacancyDTO(v));
		}
		}

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("total-pages", "" + totalPages);
		httpHeaders.add("total-elements", "" + totalElements);
		return new ResponseEntity<>(vacancies, httpHeaders, HttpStatus.OK);

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<VacancyDTO> getVacancy(@PathVariable Long id) {
		Vacancy vacancy = vacancyService.findOne(id);
		if (vacancy == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(new VacancyDTO(vacancy), HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<VacancyDTO> deleteVacancy(@PathVariable Long id) {
		Vacancy vacancy = vacancyService.findOne(id);
		if (vacancy != null) {

			vacancyService.remove(id);
			return new ResponseEntity<>(new VacancyDTO(vacancy), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<VacancyDTO> addVacancy(
			@RequestBody VacancyDTO vacancyDto) {

		Vacancy vacancy = new Vacancy();
		vacancy.setJobTitle(vacancyDto.getJobTitle());
		vacancy.setCompanyName(vacancyDto.getCompanyName());
		vacancy.setExpiringDate(vacancyDto.getExpiringDate());
		vacancy.setCreationDate(vacancyDto.getCreationDate());
		vacancy.setJobDescription(vacancyDto.getJobDescription());

		Vacancy vacancyPersisted = vacancyService.save(vacancy);

		return new ResponseEntity<>(new VacancyDTO(vacancyPersisted),
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = "application/json")
	public ResponseEntity<VacancyDTO> editVacancy(@PathVariable Long id,
			@RequestBody VacancyDTO vacancyDto) {

		Vacancy vacancy = vacancyService.findOne(id);
		if (vacancy != null) {
			if (id != vacancyDto.getId()) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}

			vacancy.setJobTitle(vacancyDto.getJobTitle());
			vacancy.setCompanyName(vacancyDto.getCompanyName());
			vacancy.setExpiringDate(vacancyDto.getExpiringDate());
			vacancy.setCreationDate(vacancyDto.getCreationDate());
			vacancy.setJobDescription(vacancyDto.getJobDescription());

			Vacancy persistedVacancy = vacancyService.save(vacancy);

			return new ResponseEntity<>(new VacancyDTO(persistedVacancy),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	// @RequestMapping(method = RequestMethod.GET)
	// public ResponseEntity<List<VacancyDTO>> getVacancies(
	// @RequestParam(value = "name", required = false) String name,
	// @RequestParam(value = "page", required = true) int page,
	// @RequestParam(value = "pageSize", required = true) int pageSize) {
	// List<VacancyDTO> vacancies = new ArrayList<>();
	// int totalPages = 0;
	// long totalElements = 0;
	//
	// if (name == null) {
	// Page<Vacancy> vacanciesPage = vacancyService
	// .findAll(page, pageSize);
	// for (Vacancy v : vacanciesPage) {
	// vacancies.add(new VacancyDTO(v));
	// }
	// totalPages = vacanciesPage.getTotalPages();
	// totalElements = vacanciesPage.getTotalElements();
	// } else {
	// for (Vacancy v : vacancyService.findByJobTitle(name)) {
	// vacancies.add(new VacancyDTO(v));
	// }
	// }
	//
	// HttpHeaders httpHeaders = new HttpHeaders();
	// httpHeaders.add("total-pages", "" + totalPages);
	// httpHeaders.add("total-elements", "" + totalElements);
	//
	// return new ResponseEntity<>(vacancies, httpHeaders, HttpStatus.OK);
	// }
	//
	// @RequestMapping(value = "/{id}", method = RequestMethod.GET)
	// public ResponseEntity<VacancyDTO> getVacancy(@PathVariable Long id) {
	// Vacancy vacancy = vacancyService.findOne(id);
	// if (vacancy == null) {
	// return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	// }
	//
	// return new ResponseEntity<>(new VacancyDTO(vacancy), HttpStatus.OK);
	// }
	//
	// @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	// public ResponseEntity<VacancyDTO> deleteVacancy(@PathVariable Long id) {
	// Vacancy vacancy = vacancyService.findOne(id);
	// if (vacancy != null) {
	//
	// vacancyService.remove(id);
	// return new ResponseEntity<>(new VacancyDTO(vacancy), HttpStatus.OK);
	// } else {
	// return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	// }
	// }
	//
	// @RequestMapping(method = RequestMethod.POST, consumes =
	// "application/json")
	// public ResponseEntity<VacancyDTO> addVacancy(
	// @RequestBody VacancyDTO vacancyDto) {
	//
	// Vacancy vacancy = new Vacancy();
	// vacancy.setJobTitle(vacancyDto.getJobTitle());
	// vacancy.setCompanyName(vacancyDto.getCompanyName());
	// vacancy.setExpiringDate(vacancyDto.getExpiringDate());
	// vacancy.setCreationDate(vacancyDto.getCreationDate());
	// vacancy.setJobDescription(vacancyDto.getJobDescription());
	//
	// Vacancy vacancyPersisted = vacancyService.save(vacancy);
	//
	// return new ResponseEntity<>(new VacancyDTO(vacancyPersisted),
	// HttpStatus.CREATED);
	// }
	//
	// @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes =
	// "application/json")
	// public ResponseEntity<VacancyDTO> editVacancy(@PathVariable Long id,
	// @RequestBody VacancyDTO vacancyDto) {
	//
	// Vacancy vacancy = vacancyService.findOne(id);
	// if (vacancy != null) {
	// if (id != vacancyDto.getId()) {
	// return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	// }
	//
	// vacancy.setJobTitle(vacancyDto.getJobTitle());
	// vacancy.setCompanyName(vacancyDto.getCompanyName());
	// vacancy.setExpiringDate(vacancyDto.getExpiringDate());
	// vacancy.setCreationDate(vacancyDto.getCreationDate());
	// vacancy.setJobDescription(vacancyDto.getJobDescription());
	//
	// Vacancy persistedVacancy = vacancyService.save(vacancy);
	//
	// return new ResponseEntity<>(new VacancyDTO(persistedVacancy),
	// HttpStatus.OK);
	// } else {
	// return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	// }
	// }

}
