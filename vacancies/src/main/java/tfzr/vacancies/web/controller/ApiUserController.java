package tfzr.vacancies.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tfzr.vacancies.web.dto.UserDTO;
import tfzr.vacancies.model.User;
import tfzr.vacancies.service.UserService;

@RestController
@RequestMapping("api/users")
public class ApiUserController {

	@Autowired
	public UserService userService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<UserDTO>> getUsers(
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "pageSize", required = true) int pageSize) {
		List<UserDTO> users = new ArrayList<>();
		int totalPages = 0;
		long totalElements = 0;

		if (email == null) {
			Page<User> usersPage = userService.findAll(page, pageSize);
			for (User u : usersPage) {
				users.add(new UserDTO(u));
			}
			totalPages = usersPage.getTotalPages();
			totalElements = usersPage.getTotalElements();
		}

		else {
			for (User u : userService.findByEmail(email)) {
				users.add(new UserDTO(u));
			}

		}
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("total-pages", "" + totalPages);
		httpHeaders.add("total-elements", "" + totalElements);
		return new ResponseEntity<>(users, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<UserDTO> getUser(@PathVariable Long id) {
		User user = userService.findOne(id);
		if (user == null) {
			return new ResponseEntity<UserDTO>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<UserDTO>(new UserDTO(user), HttpStatus.OK);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public ResponseEntity<UserDTO> deleteUser(@PathVariable Long id) {
		User user = userService.findOne(id);
		if (user != null) {
			userService.remove(id);
			return new ResponseEntity<UserDTO>(new UserDTO(user), HttpStatus.OK);
		} else {
			return new ResponseEntity<UserDTO>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<UserDTO> addUser(@RequestBody UserDTO userDto) {

		User user = new User();
		user.setId(userDto.getId());
		user.setEmail(userDto.getEmail());
		user.setPassword(userDto.getPassword());
		user.setFirstname(userDto.getFirstname());
		user.setLastname(userDto.getLastname());

		User userPersister = userService.save(user);
		return new ResponseEntity<UserDTO>(new UserDTO(userPersister),
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT, consumes = "application/json")
	public ResponseEntity<UserDTO> editUser(@PathVariable Long id,
			@RequestBody UserDTO userDto) {
		User user = userService.findOne(id);
		if (userService.findOne(id) != null) {
			if (id != userDto.getId()) {
				return new ResponseEntity<UserDTO>(HttpStatus.BAD_REQUEST);
			}

			user.setId(userDto.getId());
			user.setEmail(userDto.getEmail());
			user.setPassword(userDto.getPassword());
			user.setFirstname(userDto.getFirstname());
			user.setLastname(userDto.getLastname());

			User userPersisted = userService.save(user);

			return new ResponseEntity<UserDTO>(new UserDTO(userPersisted),
					HttpStatus.OK);

		} else {
			return new ResponseEntity<UserDTO>(HttpStatus.NOT_FOUND);
		}
	}

	// @RequestMapping(method = RequestMethod.GET)
	// public ResponseEntity<List<UserDTO>> getUsers(
	// @RequestParam(value = "value", required = false) String value,
	// @RequestParam(value = "page", required = true) int page) {
	// List<UserDTO> users = new ArrayList<>();
	// int totalPages = 0;
	// long totalElements = 0;
	// int totalPagesSearch = 0;
	// long totalElementsSearch = 0;
	// HttpHeaders httpHeaders = new HttpHeaders();
	//
	// if (value == null) {
	// Page<User> usersPage = userService.findAll(page);
	// for (User u : usersPage) {
	// users.add(new UserDTO(u));
	// }
	// totalPages = usersPage.getTotalPages();
	// totalElements = usersPage.getTotalElements();
	//
	// httpHeaders.add("total-pages", "" + totalPages);
	// httpHeaders.add("total-elements", "" + totalElements);
	//
	// } else {
	// Page<User> usersPageSearch = userService.findByEmail(value, page);
	// for (User u : userService.findByEmail(value, page)) {
	// users.add(new UserDTO(u));
	// }
	// totalPagesSearch = usersPageSearch.getTotalPages();
	// totalElementsSearch = usersPageSearch.getTotalElements();
	//
	// httpHeaders.add("total-pages", "" + totalPagesSearch);
	// httpHeaders.add("total-elements", "" + totalElementsSearch);
	// }
	//
	// return new ResponseEntity<List<UserDTO>>(users, httpHeaders,
	// HttpStatus.OK);
	// }
	//
	// @RequestMapping(value = "/{id}", method = RequestMethod.GET)
	// public ResponseEntity<UserDTO> getUser(@PathVariable Long id) {
	// User user = userService.findOne(id);
	// if (user == null) {
	// return new ResponseEntity<UserDTO>(HttpStatus.NOT_FOUND);
	// }
	// return new ResponseEntity<UserDTO>(new UserDTO(user), HttpStatus.OK);
	// }
	//
	// @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	// public ResponseEntity<UserDTO> deleteUser(@PathVariable Long id) {
	// User user = userService.findOne(id);
	// if (user != null) {
	// userService.remove(id);
	// return new ResponseEntity<UserDTO>(new UserDTO(user), HttpStatus.OK);
	// } else {
	// return new ResponseEntity<UserDTO>(HttpStatus.NOT_FOUND);
	// }
	// }
	//
	// @RequestMapping(method = RequestMethod.POST, consumes =
	// "application/json")
	// public ResponseEntity<UserDTO> addUser(@RequestBody UserDTO userDto) {
	//
	// User user = new User();
	// user.setId(userDto.getId());
	// user.setEmail(userDto.getEmail());
	// user.setPassword(userDto.getPassword());
	// user.setFirstname(userDto.getFirstname());
	// user.setLastname(userDto.getLastname());
	//
	// User userPersister = userService.save(user);
	// return new ResponseEntity<UserDTO>(new UserDTO(userPersister),
	// HttpStatus.CREATED);
	// }
	//
	// @RequestMapping(value = "{id}", method = RequestMethod.PUT, consumes =
	// "application/json")
	// public ResponseEntity<UserDTO> editUser(@PathVariable Long id,
	// @RequestBody UserDTO userDto) {
	//
	// if (userService.findOne(id) != null) {
	// if (id != userDto.getId()) {
	// return new ResponseEntity<UserDTO>(HttpStatus.BAD_REQUEST);
	// }
	//
	// User user = userService.findOne(id);
	// user.setId(userDto.getId());
	// user.setEmail(userDto.getEmail());
	// user.setPassword(userDto.getPassword());
	// user.setFirstname(userDto.getFirstname());
	// user.setLastname(userDto.getLastname());
	//
	// User userPersister = userService.save(user);
	//
	// return new ResponseEntity<UserDTO>(new UserDTO(userPersister),
	// HttpStatus.OK);
	//
	// } else {
	// return new ResponseEntity<UserDTO>(HttpStatus.NOT_FOUND);
	// }
	// }
}
