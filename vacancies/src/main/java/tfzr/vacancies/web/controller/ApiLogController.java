package tfzr.vacancies.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tfzr.vacancies.model.Log;
import tfzr.vacancies.model.User;
import tfzr.vacancies.model.Vacancy;
import tfzr.vacancies.service.LogService;
import tfzr.vacancies.service.UserService;
import tfzr.vacancies.service.VacancyService;
import tfzr.vacancies.web.dto.LogDTO;


@RestController
@RequestMapping("api/logs")
public class ApiLogController {

	@Autowired
	private LogService logService;
	@Autowired
	private VacancyService vacancyService;
	@Autowired
	private UserService userService;

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<LogDTO> addLog(@RequestBody LogDTO logDto) {

		Log log = new Log();
		log.setDateApplied(logDto.getDateApplied());
		Vacancy vacancy = vacancyService.findOne(logDto.getVacancy()
				.getId());
		User user = userService.findOne(logDto.getUser().getId());
		log.setVacancy(vacancy);
		log.setUser(user);

		Log logPersisted = logService.save(log);

		return new ResponseEntity<>(new LogDTO(logPersisted),
				HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<LogDTO>> getLogs(
			@RequestParam(value = "vacancyId", required = false) Long vacancyId,
			@RequestParam(value = "userId", required = false) Long userId) {

		List<LogDTO> logs = new ArrayList<>();

		if (vacancyId != null) {
			Vacancy vacancy = vacancyService.findOne(vacancyId);
			for (Log log : vacancy.getLogs()) {
				logs.add(new LogDTO(log));
			}
		} else if (userId != null) {
			User user = userService.findOne(userId);
			for (Log log : user.getLogs()) {
				logs.add(new LogDTO(log));
			}
		} else {
			List<Log> logs2 = logService.findAll();
			for (Log log : logs2) {
				logs.add(new LogDTO(log));
			}
		}

		return new ResponseEntity<>(logs, HttpStatus.OK);
	}
}